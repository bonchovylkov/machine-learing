﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearing.Console
{
    public class Perceptron
    {

        private static Random rnd;

        public double[] Weights { get; set; }

        public int[,] Inputs { get; set; }

        public int[] Outputs { get; set; }

        public int InputCount
        {
            get
            {
                return this.Inputs.GetLength(0);
            }
        }

        public int FeaturesCount
        {
            get
            {
                return this.Inputs.GetLength(1);
            }
        }

        private const double Bias = 1;

        //for perceptron is not mandatory
        public double LearningRate { get; set; }

        public double TotalError { get; set; }

        public Perceptron(int[,] inputData, int[] outputData)
        {
            //by default for now
            LearningRate = 1;
            TotalError = 1;

            //TODO: add validation fot the lengths of input and output datas

            this.Inputs = new int[inputData.GetLength(0), inputData.GetLength(1)];


            for (int i = 0; i < inputData.GetLength(0); i++)
            {
                for (int j = 0; j < inputData.GetLength(1); j++)
                {
                    this.Inputs[i, j] = inputData[i, j];
                }
            }

            this.Outputs = new int[outputData.Length];

            for (int i = 0; i < outputData.Length; i++)
            {
                this.Outputs[i] = outputData[i];
            }

            rnd = new Random();

            //plus one because of the Bias weight
            //init the weights length with count of the features in each input
            Weights = new double[FeaturesCount + 1];

            for (int i = 0; i < Weights.Length; i++)
            {
                Weights[i] = rnd.NextDouble();
            }
        }


        public int Train()
        {
            int trainingSteps = 0;


            while (TotalError >0.2)
            {
                trainingSteps++;

                this.TotalError = 0;

                for (int i = 0; i < InputCount; i++)
                {
                    //this makes sum ot pruduct of input * weigth
                    double output = CalculateOuput(i, Inputs);

                    double currentError = Outputs[i] - output;

                    for (int j = 0; j < Weights.Length-1; j++)
                    {
                        Weights[j] += LearningRate * Inputs[i,j] * currentError; //LearningRate doesn't change anything currently
                    }

                    //update the bias weight separatly
                    Weights[Weights.Length - 1] += LearningRate * 1 * currentError;

                    this.TotalError += Math.Abs(currentError);
                    
                }

                System.Console.WriteLine(TotalError);

            }

            return trainingSteps;
        }

        /// <summary>
        /// Calculate output by given global weights and input data parameters
        /// </summary>
        /// <param name="inputIndex"></param>
        /// <param name="localInputData"></param>
        /// <returns></returns>
        public double CalculateOuput(int inputIndex, int[,] localInputData )
        {
            double output = 0;

            for (int featureIndex = 0; featureIndex < FeaturesCount; featureIndex++)
            {
                var currentFeatureValue = localInputData[inputIndex, featureIndex];
                var currentWeigthValue = Weights[featureIndex];
                output += currentFeatureValue * currentWeigthValue;
            }

            output += Bias * Weights[Weights.Length - 1];

            //activation function
            return output >= 0 ? 1 : 0;
        }



    }
}
