﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearing.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var inputData = new double[,] { { 0, 0, 1 }, { 0, 1, 1 }, { 1, 0, 1 }, { 1, 1, 1 }, { 0, 0, 0 }, { 1, 1, 0 } };

            var output = Transpose(new double[,] { { 0, 1, 1, 0, 0, 1 } }); //np.array([[0, 1, 1, 0]]).T

            //TODO: find out what it means...
            //var syn0 = new double[,]{{-0.06986369 , 0.09441766 , 0.44641062 ,-0.20027054},
            //                         {0.69308447,  0.16227149 ,-0.1311596,   0.36183623},
            //                         {0.71240643, -0.40352232, -0.78812653, -0.62850569}};


            ////TODO: find out what it means...
            //var syn1 = new double[,]{{0.03737035},
            //                                     {-0.61573329},
            //                                     {0.34430937},
            //                                     {0.70037969}};

            var syn0 = CreateRandomWeights(inputData.GetLength(1), inputData.GetLength(0)); //rows and cols are reversed


            //TODO: find out what it means...
            var syn1 = CreateRandomWeights(output.GetLength(0), output.GetLength(1));

            //train
            for (int i = 0; i < 50000; i++)
            {
                var layer1 = SimgoidActivation(inputData, syn0);
                var layer2 = SimgoidActivation(layer1, syn1);

                //(y - l2)*(l2*(1-l2))
                var layer2_delta = CalculateHiddenLayerError(output, layer2);

                //l2_delta.dot(syn1.T) * (l1 * (1-l1))
                var layer1_delta = MultiplyMatricesMembers(MultiplyMatrix(layer2_delta, Transpose(syn1)), GetErrorRightPart(layer1));

                // l1.T.dot(l2_delta)
                var syn1Changes = MultiplyMatrix(Transpose(layer1), layer2_delta);
                //X.T.dot(l1_delta)
                var syn0Changes = MultiplyMatrix(Transpose(inputData), layer1_delta);



                MatrixReferenceSum(syn1, syn1Changes);
                MatrixReferenceSum(syn0, syn0Changes);
            }

            PrintMatrix(syn0);
            PrintMatrix(syn1);


            //test

            var l1 = SimgoidActivation(new double[,] { { 0,0,0 } }, syn0);
            var l2 = SimgoidActivation(l1, syn1);
            PrintMatrix(l2);

            //testring
            //var layer1 = SimgoidActivation(inputData, syn0);
            //PrintMatrix(layer1);


            //var layer2 = SimgoidActivation(layer1, syn1);
            //PrintMatrix(layer2);







            ////(y - l2)*(l2*(1-l2))
            //var layer2_delta = CalculateHiddenLayerError(output, layer2);
            //PrintMatrix(layer2_delta);

            ////l2_delta.dot(syn1.T) * (l1 * (1-l1))

            //var layer1_delta =  MultiplyMatricesMembers( MultiplyMatrix(layer2_delta,Transpose(syn1))  ,GetErrorRightPart(layer1));
            //PrintMatrix(layer1_delta);


            //// l1.T.dot(l2_delta)
            //var syn1Changes = MultiplyMatrix(Transpose(layer1),layer2_delta);
            ////X.T.dot(l1_delta)
            //var syn0Changes = MultiplyMatrix(Transpose(inputData), layer1_delta);

            //PrintMatrix(syn1Changes);
            //PrintMatrix(syn0Changes);

            //PrintMatrix(MatrixSum(syn1 ,syn1Changes));
            //PrintMatrix(MatrixSum(syn0, syn0Changes));

            //    [[ 0.71240643 -0.40352232 -0.78812653 -0.62850569]
            //[ 1.4054909  -0.24125083 -0.91928612 -0.26666946]
            //[ 0.64254274 -0.30910466 -0.34171591 -0.82877623]
            //[ 1.33562721 -0.14683317 -0.4728755  -0.46694   ]]

        }


        public static double[,] CreateRandomWeights(int r, int c)
        {
            Random rnd = new Random();
            double[,] weights = new double[r, c];

            for (int i = 0; i < r; i++)
            {
                for (int j = 0; j < c; j++)
                {
                    weights[i, j] = rnd.NextDouble() - rnd.NextDouble();
                }
            }

            return weights;
        }

        /// <summary>
        /// Find this imperical by testing numpy array multiplication of matrices
        /// </summary>
        /// <param name="m1"></param>
        /// <returns></returns>
        public static double[,] GetDiagonal(double[,] m1)
        {
            double[,] result = new double[m1.GetLength(0), 1];


            for (int rowNum = 0; rowNum < m1.GetLength(0); rowNum++)
            {
                for (int colNum = 0; colNum < 1; colNum++)
                {
                    result[rowNum, colNum] = m1[rowNum, rowNum];
                }
            }

            return result;
        }

        //public  MyProperty { get; set; }

        public static void PrintMatrix(double[,] m)
        {
            System.Console.WriteLine(new string('*', 50));
            for (int rowNum = 0; rowNum < m.GetLength(0); rowNum++)
            {
                //gets the row on given position
                object[] row = Enumerable.Range(0, m.GetLength(1))
                         .Select(colNum => (object)m[rowNum, colNum])
                         .ToArray();
                System.Console.WriteLine(string.Join(",", row));
            }
            System.Console.WriteLine(new string('*', 50));
        }

        private static void TestPerceptron()
        {
            //learn the perceptron ot recognize And operator (&&)
            var inputData = new int[,] { { 1, 0 }, { 1, 1 }, { 0, 1 }, { 0, 0 } };
            int[] outputData = { 0, 1, 0, 0 };
            var perceptron = new Perceptron(inputData, outputData);

            System.Console.WriteLine(perceptron.Train());

            var test1 = perceptron.CalculateOuput(0, new int[,] { { 1, 0 } });
            var test2 = perceptron.CalculateOuput(0, new int[,] { { 1, 1 } });
            var test3 = perceptron.CalculateOuput(0, new int[,] { { 0, 1 } });
            var test4 = perceptron.CalculateOuput(0, new int[,] { { 0, 0 } });

            System.Console.WriteLine("{ 1, 0 } = 0 " + (test1 == 0));
            System.Console.WriteLine("{ 1, 1 } = 1 " + (test2 == 1));
            System.Console.WriteLine("{ 0, 1 } = 0 " + (test3 == 0));
            System.Console.WriteLine("{ 0, 0 } = 0 " + (test4 == 0));

            //learn the perceptron ot recognize Or operator (||)

            var inputDataOr = new int[,] { { 1, 0 }, { 1, 1 }, { 0, 1 }, { 0, 0 } };
            int[] outputDataOr = { 1, 1, 1, 0 };
            var perceptronOr = new Perceptron(inputDataOr, outputDataOr);

            System.Console.WriteLine(perceptronOr.Train());

            var testOr1 = perceptronOr.CalculateOuput(0, new int[,] { { 1, 0 } });
            var testOr2 = perceptronOr.CalculateOuput(0, new int[,] { { 1, 1 } });
            var testOr3 = perceptronOr.CalculateOuput(0, new int[,] { { 0, 1 } });
            var testOr4 = perceptronOr.CalculateOuput(0, new int[,] { { 0, 0 } });

            System.Console.WriteLine("{ 1, 0 } = 1 " + (testOr1 == 1));
            System.Console.WriteLine("{ 1, 1 } = 1 " + (testOr2 == 1));
            System.Console.WriteLine("{ 0, 1 } = 1 " + (testOr3 == 1));
            System.Console.WriteLine("{ 0, 0 } = 0 " + (testOr4 == 0));
        }

        public static double[,] MultiplyMatrix(double[,] A, double[,] B)
        {
            int rowsA = A.GetLength(0);
            int columnsA = A.GetLength(1);
            int rowsB = B.GetLength(0);
            int colsB = B.GetLength(1);
            double temp = 0;
            double[,] kHasil = new double[rowsA, colsB];
            if (columnsA != rowsB)
            {


                System.Console.WriteLine("matrix can't be multiplied !!");
                throw new ArgumentException("matrix can't be multiplied !!");
            }

            for (int i = 0; i < rowsA; i++)
            {
                for (int j = 0; j < colsB; j++)
                {
                    temp = 0;
                    for (int k = 0; k < columnsA; k++)
                    {
                        temp += A[i, k] * B[k, j];
                    }
                    kHasil[i, j] = temp;
                }
            }
            return kHasil;

        }


        public static void InvertValuesInMatrix(double[,] m)
        {
            for (int rowNum = 0; rowNum < m.GetLength(0); rowNum++)
            {
                for (int colNum = 0; colNum < m.GetLength(1); colNum++)
                {
                    m[rowNum, colNum] = -1 * m[rowNum, colNum];
                }
            }
        }


        public static double[,] SimgoidActivation(double[,] inputs, double[,] weights)
        {
            double[,] result = MultiplyMatrix(inputs, weights);
            InvertValuesInMatrix(result);

            for (int rowNum = 0; rowNum < result.GetLength(0); rowNum++)
            {
                for (int colNum = 0; colNum < result.GetLength(1); colNum++)
                {
                    //result[rowNum, colNum] = 1 / (1 + Math.Pow(Math.E, result[rowNum, colNum]));
                    result[rowNum, colNum] = 1 / (1 + EulerNumberOnPower(result[rowNum, colNum]));
                }
            }

            return result;
        }


        public static double[,] MatrixDifference(double[,] m1, double[,] m2)
        {
            double[,] result = new double[m1.GetLength(0), m1.GetLength(1)];


            for (int rowNum = 0; rowNum < result.GetLength(0); rowNum++)
            {
                for (int colNum = 0; colNum < result.GetLength(1); colNum++)
                {
                    result[rowNum, colNum] = m1[rowNum, colNum] - m2[rowNum, colNum];
                }
            }

            return result;
        }

        public static double[,] MatrixSum(double[,] m1, double[,] m2)
        {
            double[,] result = new double[m1.GetLength(0), m1.GetLength(1)];


            for (int rowNum = 0; rowNum < result.GetLength(0); rowNum++)
            {
                for (int colNum = 0; colNum < result.GetLength(1); colNum++)
                {
                    result[rowNum, colNum] = m1[rowNum, colNum] + m2[rowNum, colNum];
                }
            }

            return result;
        }


        /// <summary>
        /// Updates the first matrix my the values of the second
        /// </summary>
        /// <param name="m1">Updated matrix</param>
        /// <param name="m2">The other matrix to values from</param>
        public static void MatrixReferenceSum(double[,] m1, double[,] m2)
        {



            for (int rowNum = 0; rowNum < m1.GetLength(0); rowNum++)
            {
                for (int colNum = 0; colNum < m1.GetLength(1); colNum++)
                {
                    m1[rowNum, colNum] = m1[rowNum, colNum] + m2[rowNum, colNum];
                }
            }

        }

        public static double[,] CopyMatrix(double[,] m1)
        {
            double[,] copy = new double[m1.GetLength(0), m1.GetLength(1)];


            for (int rowNum = 0; rowNum < copy.GetLength(0); rowNum++)
            {
                for (int colNum = 0; colNum < copy.GetLength(1); colNum++)
                {
                    copy[rowNum, colNum] = m1[rowNum, colNum];
                }
            }

            return copy;
        }


        public static double EulerNumberOnPower(double number)
        {
            return Math.Pow(Math.E, number);
        }

        /// <summary>
        /// Test method, it's not needed for the algorithm
        /// </summary>
        /// <param name="m"></param>
        public static void EulerPowerMatrix(double[,] m)
        {
            for (int rowNum = 0; rowNum < m.GetLength(0); rowNum++)
            {
                for (int colNum = 0; colNum < m.GetLength(1); colNum++)
                {
                    //result[rowNum, colNum] = 1 / (1 + Math.Pow(Math.E, result[rowNum, colNum]));
                    m[rowNum, colNum] = 1 / (1 + EulerNumberOnPower(m[rowNum, colNum]));
                }
            }
        }


        public static double[,] Transpose(double[,] matrix)
        {
            var transosedMatrix = new double[matrix.GetLength(1), matrix.GetLength(0)];

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {

                    transosedMatrix[j, i] = matrix[i, j];
                }
            }


            return transosedMatrix;
        }

        public static double[,] CreateMatrixByDifferenceOfNumber(double[,] m, double number)
        {
            double[,] newMatrix = new double[m.GetLength(0), m.GetLength(1)];

            for (int rowNum = 0; rowNum < m.GetLength(0); rowNum++)
            {
                for (int colNum = 0; colNum < m.GetLength(1); colNum++)
                {
                    newMatrix[rowNum, colNum] = number - m[rowNum, colNum];
                }
            }

            return newMatrix;
        }

        /// <summary>
        /// Calculate error by the formula - (y - l2)*(l2*(1-l2))
        /// </summary>
        /// <param name="realOutput"></param>
        /// <param name="currentOutput"></param>
        /// <returns></returns>
        public static double[,] CalculateHiddenLayerError(double[,] realOutput, double[,] currentOutput)
        {
            var difference = MatrixDifference(realOutput, currentOutput); //(y - l2)


            double[,] multipliedMatrices = GetErrorRightPart(currentOutput); //(l2*(1-l2))


            var result = MultiplyMatricesMembers(difference, multipliedMatrices); //(y - l2)*(l2*(1-l2))

            return result;
        }

        /// <summary>
        /// this is (l1 * (1-l1)) the part from (y - l2)*(l2*(1-l2))
        /// </summary>
        /// <param name="currentOutput"></param>
        /// <returns></returns>
        private static double[,] GetErrorRightPart(double[,] currentOutput)
        {
            var oneMinusMatrix = CreateMatrixByDifferenceOfNumber(currentOutput, 1);
            var multipliedMatrices = MultiplyMatricesMembers(currentOutput, oneMinusMatrix);
            return multipliedMatrices;
        }


        public static double[,] MultiplyMatricesMembers(double[,] m1, double[,] m2)
        {
            if (m1.GetLength(0) != m2.GetLength(0) || m1.GetLength(1) != m2.GetLength(1))
            {
                throw new ArgumentException("cannot multiply matrices members with different dimentions");
            }

            double[,] result = new double[m1.GetLength(0), m1.GetLength(1)];


            for (int rowNum = 0; rowNum < result.GetLength(0); rowNum++)
            {
                for (int colNum = 0; colNum < result.GetLength(1); colNum++)
                {
                    result[rowNum, colNum] = m1[rowNum, colNum] * m2[rowNum, colNum];
                }
            }

            return result;
        }
    }
}
